Name:           libcdio-paranoia
Version:        10.2+2.0.2
Release:        1
Summary:        CD paranoia on top of libcdio
License:        GPLv3+
URL:            https://www.gnu.org/software/libcdio/
Source0:        https://ftp.gnu.org/gnu/libcdio/%{name}-%{version}.tar.bz2
BuildRequires:  gcc pkgconfig gettext-devel chrpath libcdio-devel

%description
This CDDA reader distribution ('libcdio-cdparanoia') reads audio from the
CDROM directly as data, with no analog step between, and writes the
data to a file or pipe as .wav, .aifc or as raw 16 bit linear PCM.

%package        devel
Summary:        Header files and libraries for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
This package contains header files and libraries for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

sed -i -e 's,-I${includedir},-I${includedir}/cdio,g' libcdio_paranoia.pc.in
sed -i -e 's,-I${includedir},-I${includedir}/cdio,g' libcdio_cdda.pc.in

f=doc/ja/cd-paranoia.1.in
iconv -f euc-jp -t utf-8 -o $f.utf8 $f && mv $f.utf8 $f
iconv -f ISO88591 -t utf-8 -o THANKS.utf8 THANKS && mv THANKS.utf8 THANKS

%build
%configure --disable-dependency-tracking --disable-rpath
%make_build

%install
%make_install

%delete_la

cp -a $RPM_BUILD_ROOT%{_includedir}/cdio/paranoia/*.h $RPM_BUILD_ROOT%{_includedir}/cdio/

chrpath --delete $RPM_BUILD_ROOT%{_bindir}/*
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/*.so.*

%check
make %{?_smp_mflags} check

%files
%defattr(-,root,root)
%doc AUTHORS
%license COPYING
%{_bindir}/*
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/cdio/*
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/*.pc

%files  help
%defattr(-,root,root)
%doc doc/overlapdef.txt README.md THANKS
%{_mandir}/man1/*
%lang(ja) %{_mandir}/ja/man1/*

%changelog
* Thu Oct 24 2024 GuoCe <guoce@kylinos.cn> - 10.2+2.0.2-1
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: update to version 10.2+2.0.2

* Tue Mar 12 2024 liweigang <liweiganga@uniontech.com> - 10.2+2.0.1-1
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: update to version 10.2+2.0.1

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 10.2+2.0.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: optimization the spec

* Thu Sep 05 2019 openEuler Buildteam <buildteam@openeuler.org> - 10.2+2.0.0-1
- Package init
